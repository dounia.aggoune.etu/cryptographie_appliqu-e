#!/usr/bin/python3
# coding: utf-8

import banque
import client
import commercant
import os
import os.path
from os import path

# Code pour tester les fonctionnalités 

def test_1():
    # initialise folders, files and key pair for bank
    banque.initBanque()

    stop = True
    while stop:
        action = input("Entrez 0 pour quitter ou entrez le numéro de l'action désirée :\n1. Création compte client\n2. Création compte commerçant\n3. Générer une facture\n4. Générer un chèque\n5. Vérifier la validité d'un chèque\n6. Encaissement de chèque(s)\n7. Consulter la liste des factures d'un client \n8. Consulter la liste des chèques d'un commerçant\n9. Consulter le contenu d'une facture d'un client\n10. Consulter le contenu d'un chèque d'un commerçant\n")
        try:
            int(action)
        except ValueError:
            print("Action inconnue \n")
        else:
            if int(action) == 1: # Création compte client
                banque.creerUtilisateur("client")
            
            elif int(action) == 2: # Création compte commerçant
                banque.creerUtilisateur("commercant")
            
            elif int(action) == 3: # generer une facture
                montant = input("Entrer le montant de la facture \n")
                idCommercant = input("Entrer le nom du commercant \n")
                idClient = input("Entrer le nom du client \n")
                # On génère la facture
                pathFacture = commercant.genererFacture(montant, idCommercant, idClient)
                print("NOTIFICATION sur l'app du client : " + idClient + " : Vous avez reçu une nouvelle facture.\n")
           
            elif int(action) == 4: # Générer un chèque
                montant = input("Entrer le montant du chèque \n")
                idCommercant = input("Entrer le nom du commercant \n")
                idClient = input("Entrer le nom du client \n")
                numFacture = input("Entrer le numéro de la facture \n")
                pathfacture = "Client/" + str(idClient) + "/facture-" + str(numFacture) + ".txt"
                pathCheque = client.genererCheque(pathfacture, idClient, idCommercant)
            
            elif int(action) == 5: # Vérifier la validité d'un chèque
                if commercant.verifierCheque():
                    print("Le chèque est valide")
                else:
                    print("Le chèque est invalide")
            
            
            elif int(action) == 6: # Encaissement de chèque(s)
                print("entrez le nombre de chèques à encaisser \n")
                nb = input()
                for i in range(int(nb)):
                    idC = input("Entrer le nom du commerçant \n")
                    num = input("Entrer le numéro du chèque \n")
                    banque.encaisserCheques("Commercant/" + idC + "/cheque-" + num + ".txt")
            
            elif int(action) == 7: # Consulter la liste des factures d'un client
                nom = input("Entrer le nom du client\n")
                if path.isdir("Client/" + str(nom)): # checks if path exists
                    cmd = "ls Client/" + str(nom) + " | grep \"facture-\""
                    print("---------------------\n")
                    os.system(cmd)
                    print("---------------------\n")
                else:
                    print("Ce nom d'utilisateur n'existe pas")
            
            elif int(action) == 8: # Consulter la liste des chèques d'un commerçant
                nom = input("Entrer le nom du commerçant\n")
                if path.isdir("Commercant/" + str(nom)): # checks if path exists
                    cmd = "ls Commercant/" + str(nom) + " | grep \"cheque-\""
                    print("---------------------\n")
                    os.system(cmd)
                    print("---------------------\n")
                else:
                    print("Ce nom d'utilisateur n'existe pas")
            
            elif int(action) == 9: # Consulter le contenu d'une facture d'un client
                nom = input("Entrer le nom du client\n")
                num = input("Entrer le numéro de la facture\n")
                if path.isfile("Client/" + str(nom) + "/facture-" + str(num) + ".txt"): # checks if file exists
                    cmd = "cat Client/" + str(nom) + "/facture-" + str(num) + ".txt"
                    print("---------------------\n")
                    os.system(cmd)
                    print("---------------------\n")
                else:
                    print("Cette facture n'existe pas")
            
            elif int(action) == 10: # Consulter le contenu d'un chèque d'un commerçant
                nom = input("Entrer le nom du commerçant\n")
                num = input("Entrer le numéro du chèque\n")
                if path.isfile("Commercant/" + str(nom) + "/cheque-" + str(num) + ".txt"): # checks if file exists
                    cmd = "cat Commercant/" + str(nom) + "/cheque-" + str(num) + ".txt"
                    print("---------------------\n")
                    os.system(cmd)
                    print("---------------------\n")
                else:
                    print("Ce chèque n'existe pas")
            elif int(action) == 0:
                stop = False
            else:
                print("Action inconnue \n")



def main():
    test_1()

if __name__ == "__main__":
    main()