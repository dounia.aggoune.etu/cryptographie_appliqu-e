#!/usr/bin/env python
# coding: utf-8

import os
import common
import sys

def getClePubSignedByBank(idClient):
    # On ouvre et lit le fichier contenant le prochain numéro de facture a utiliser 
    try:
        file = open("Client/" + str(idClient) + "/clePublicSignee.txt", "r")
        signedkey = file.read()
    except IOError:
        print("Erreur durant l'ouverture ou lecture du fichier 9")
        sys.exit()
    else:
        file.close()
        return signedkey


def getNumCheque(idClient):
    if os.path.isfile("Client/" + str(idClient) + "/numCheque.txt") == True:
        # On ouvre et lit le fichier contenant le prochain numéro de cheque a utiliser 
        try:
            numChequeFichier = open("Client/" + str(idClient) + "/numCheque.txt", "r")
            numCheque = numChequeFichier.read()
        except IOError:
            print("Erreur durant l'ouverture ou lecture du fichier 10")
            sys.exit()
        else:
            numChequeFichier.close()

        # On incrémente pour le prochain numéro de cheque
        numCheque = int(numCheque)
        numCheque+=1
        try:
            numChequeFichier = open("Client/" + str(idClient) + "/numCheque.txt", "w")
            numChequeFichier.write(str(numCheque))
        except IOError:
            print("Erreur durant l'ouverture ou écriture du fichier 11")
            sys.exit()
        else:
            numChequeFichier.close()
            return numCheque
    else:
        print("le fichier " + "Client/" + str(idClient) + "/numCheque.txt n'existe pas")
        return 0



def genererCheque(facture, idClient, idCommercant):
    print("Vous allez générer un chèque pour la facture " + facture + "\n")
    # On lit la facture
    try:
        facture = open(facture, "r")
        cpt = 1
        while True:
            line = facture.readline()
            if cpt == 1:
                numFacture = str(line).strip()
            elif cpt == 2:
                idCommercant = str(line).strip()
            elif cpt == 3:
                montant = str(line).strip()
            if not line: 
                break
            cpt+=1
    except IOError:
        print("Erreur durant l'ouverture ou lecture du fichier 13")
        sys.exit()
    else:
        facture.close()

    # On génère le fichier de chèque et on écrit les infos dedans:
    numCheque = getNumCheque(idClient)
    pubKey = common.getPubKey("Client/" + str(idClient) + "/public.pem")
    pathCheque = "Commercant/" + str(idCommercant) + "/cheque-" + str(numCheque) + ".txt"
    try:
        cheque = open(pathCheque, "w")
        cheque.write(str(idClient) + "\n" + str(idCommercant) + "\n" + str(montant) + "\n" + str(numFacture) + "\n" + str(numCheque) + "\n")
        # les infos a hacher
        strToHash = str(idClient) + "\n" + str(idCommercant) + "\n" + str(montant) + "\n" + str(numCheque) + "\n" + str(numFacture) + "\n"
        # le hash en clair 
        hashDuCheque = common.hashData(strToHash)
        # le signé du hash
        signedHash = common.signerDonnee(hashDuCheque, "Client/" + str(idClient)) 
        # la cle public signée par la banque
        signedKey = getClePubSignedByBank(idClient)
        # on rajoute les infos dans le fichier de cheque
        cheque.write(str(hashDuCheque) + "\n" + str(signedHash) + "\n" + str(signedKey)  + "\n" + str(pubKey) + "\n")
    except IOError:
        print("Erreur durant l'ouverture ou écriture du fichier 14")
        sys.exit()
    else:
        cheque.close()
        print("Le chèque est generé et le fichier du chèque est : " + pathCheque)
        return pathCheque



    





