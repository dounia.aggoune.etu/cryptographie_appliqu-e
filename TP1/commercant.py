#!/usr/bin/python3
# coding: utf-8

import os
import common
import client
import sys


def verifierCheque():
    numCheque = input("Entrer le numéro du chèque \n")
    idCommercant = input("Entrer le nom du commercant \n")
    numFacture = input("Entrer le numéro de facture correspondant \n")
    montant_attendu = input("Entrer le montant de la facture \n")
    pathCheque = "Commercant/" + str(idCommercant) + "/cheque-" + str(numCheque) + ".txt"
    
    if os.path.isfile(pathCheque): # verifie si le cheque existe
        # ouvrir le cheque et prendre les informations dont on va avoir besoin
        try:
            f = open("Commercant/" + str(idCommercant) + "/cheque-" + str(numCheque) + ".txt", "r")
            cpt = 1
            hash_signé = ""
            cps = ""
            pub = ""
            while True:
                line = f.readline()
                if cpt == 1:
                    id_client = str(line).strip()
                elif cpt == 2:
                    id_commercant = str(line).strip()
                    if str(id_commercant) != str(idCommercant): return False
                elif cpt == 3:
                    montant = str(line).strip()
                    if str(montant) != str(montant_attendu): return False
                elif cpt == 4:
                    num_facture = str(line).strip()
                    if str(num_facture) != str(numFacture): return False
                elif cpt == 5:
                    num_cheque = str(line).strip()
                elif cpt == 6:
                    hash_infos = str(line).strip()
                elif cpt >= 7 and cpt < 13:
                    hash_signé+=str(line)
                elif cpt >= 13 and cpt <= 19:
                    cps+=str(line)
                elif cpt >= 20 and cpt <= 27:
                    pub+=str(line)
                if not line: 
                    break
                cpt+=1

            hash_infos = hash_infos.strip()
            hash_signé = hash_signé.strip()
            cps = cps.strip()
            pub = pub.strip()

            """
            print("hash_infos === \n" + hash_infos)
            print("hash_signé === \n" + hash_signé)
            print("cps === \n" + cps)
            print("pub === \n" + pub)
            """
        except IOError:
            print("Erreur durant l'ouverture ou lecture du fichier 20")
            sys.exit()
        else:
            f.close()
        
        # hash info en clair et verif avec hash signe
        # verifie signature des données du chèque par le client
        if common.VerifierSignature(hash_signé, hash_infos, "Client/" + id_client + "/public.pem"): 
            # verifie signature de la clé du client par la banque
            pubK = common.getPubKey("Client/" + id_client + "/public.pem") 
            if common.VerifierSignature(cps, pubK, "Commercant/public-banque.pem"):
                # on hash les donnee en clair pour comparer avec le hash des donnees present dans le cheque
                data = str(id_client) + "\n" + str(id_commercant) + "\n" + str(montant) + "\n" + str(num_cheque) + "\n" + str(num_facture) + "\n"
                hashed = common.hashData(data)
                if str(hashed) == str(hash_infos):
                    return True
                else:
                    return False
            else:
                return False
        else:
            return False
    else:
        print("Chèque introuvable")
        return False


def getNumFacture(idCommercant):
    # On ouvre et lit le fichier contenant le prochain numéro de facture a utiliser 
    if os.path.isfile("Commercant/" + str(idCommercant) + "/numFacture.txt") == True:
        try:
            facture = open("Commercant/" + str(idCommercant) + "/numFacture.txt", "r")
            numFacture = facture.readline()
        except IOError:
            print("Erreur durant l'ouverture ou lecture du fichier 15")
            sys.exit()
        else:
            facture.close()

        # On incrémente pour le prochain numéro de facture
        numFacture = int(numFacture)
        numFacture+=1
        try:
            numFactureFichier = open("Commercant/" + str(idCommercant) + "/numFacture.txt", "w")
            numFactureFichier.write(str(numFacture))
        except IOError:
            print("Erreur durant l'ouverture ou lecture du fichier 16")
            sys.exit()
        else:
            numFactureFichier.close()
            return numFacture
    else:
        print("le fichier " + "Commercant/" + str(idCommercant) + "/numFacture.txt n'existe pas")
        return 0

def genererFacture(montant, idCommercant, idClient):
    numFacture = getNumFacture(idCommercant)
    # On génère la facture
    facturePath = "Client/" + str(idClient) + "/facture-" + str(numFacture) + ".txt"
    try:
        facture = open(facturePath, "w")
        facture.write(str(numFacture) + "\n" + idCommercant + "\n" + montant + "\n")
    except IOError:
            print("Erreur durant l'ouverture ou lecture du fichier 17")
            sys.exit()
    else:
        print("la facture est generée et le numéro de la facture est : " + str(numFacture))
        facture.close()
        return facturePath


