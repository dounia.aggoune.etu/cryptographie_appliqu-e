#!/usr/bin/python3
# coding: utf-8

import os
import os.path
from os import path
import shutil
import sys

def genererPaireDeCles(idC, userType):
    if str(userType) == "client":
        folder =  "Client/"
    elif str(userType) == "commercant":
        folder = "Commercant/"
    elif str(userType) == "banque":
        folder = "Banque/"
    else:
        return -1

    if str(userType) == "banque":
        cmdGenPrivate = "openssl genrsa -des3 -passout pass:123456789 -out private.pem 2048"
        cmdGenPublic = "openssl rsa -in private.pem -outform PEM -pubout -out public.pem -passin pass:123456789"
    else:
        cmdGenPrivate = "openssl genrsa -des3 -out private.pem 2048"
        cmdGenPublic = "openssl rsa -in private.pem -outform PEM -pubout -out public.pem"
    
    os.system(cmdGenPrivate)
    os.system(cmdGenPublic)
    
    # move files in appropriate folder named by the id of the client or trader
    if str(userType) != "banque":
        if path.exists(idC) == False:
            mkdirCmd = "mkdir -p " + folder + str(idC)
            os.system(mkdirCmd)

        a = folder + str(idC)
        shutil.move("private.pem", a)
        shutil.move("public.pem", a)
    elif str(userType) == "banque":
        a = folder
        shutil.move("private.pem", a)
        shutil.move("public.pem", a)



def initUser(idC, userType):
    if str(userType) == "client":
        folder =  "Client/"
        file = "/numCheque.txt"
    elif str(userType) == "commercant":
        folder = "Commercant/"
        file = "/numFacture.txt"
    
    # génère la paire de clés
    if os.path.isfile(folder + str(idC) + "/public.pem") == False: # checks if file already exists or not
        genererPaireDeCles(idC, userType)

    # Créer le fichier contenant le prochain numéro de cheque ou facture et si non, on le créer
    if os.path.isfile(folder + str(idC) + file) == False: # checks if file already exists or not
        fichier = open(folder + str(idC) + file, "w")
        fichier.write(str("0"))
        fichier.close()



def signerDonnee(donneeASigner, pathPrivateKey):
    try:
        file = open("donneeASigner.txt", "w")
        file.write(str(donneeASigner))
    except IOError:
        print("Erreur durant l'ouverture ou écriture du fichier 4")
        sys.exit()
    else:
        file.close()
        cmd = "openssl dgst -sha256 -sign " + str(pathPrivateKey) + "/private.pem -out signedData donneeASigner.txt"
        os.system(cmd)
        cmd1 = "openssl base64 -in signedData -out signedData.txt"
        os.system(cmd1)
    
    try:
        f = open("signedData.txt", "r")
        data = f.read()
    except IOError:
        print("Erreur durant l'ouverture ou lecture du fichier 18")
        sys.exit()
    else:
        f.close()
        os.system('rm signedData donneeASigner.txt signedData.txt')
        return data



def VerifierSignature(donneeSignee, donneeEnClair, pathPubKey):
    try:
        file = open("donneeSignee.txt", "w")
        file.write(str(donneeSignee))
    except IOError:
      print("Erreur durant l'ouverture ou écriture du fichier 4")
      sys.exit()
    else:
        file.close()

    try:
        file = open("donneeEnClair.txt", "w")
        file.write(str(donneeEnClair))
    except IOError:
      print("Erreur durant l'ouverture ou écriture du fichier 4")
      sys.exit()
    else:
        file.close()
    cmd = "openssl base64 -d -in donneeSignee.txt -out donneeSigneeBin"
    os.system(cmd)
    cmd1 = "openssl dgst -sha256 -verify " + str(pathPubKey) + " -signature donneeSigneeBin donneeEnClair.txt > res.txt"
    os.system(cmd1)
    
    try:
        #os.system('cat res.txt')
        resFile = open("res.txt")
        result = resFile.readline()
        result = result.strip()
        if str(result) == "Verified OK":
            os.system('rm res.txt donneeSignee.txt donneeEnClair.txt')
            return True 
        else:
            os.system('rm res.txt donneeSigneeBin donneeSignee.txt donneeEnClair.txt')
            return False
    except IOError:
        print("Erreur durant l'ouverture ou lecture du fichier 23")
        sys.exit()
    else:
        resFile.close()


def getPubKey(path):
    try:
        f = open(str(path), "r")
        # need to strip public key of the first and last line of the file bc it's not part of the public key
        data = ""
        cpt = 1
        data = ""
        while True:
            line = f.readline()
            if cpt != 1:
                data+=line
            if not line: 
                break
            cpt+=1
        ind1 = data.find('-----BEGIN PUBLIC KEY-----')
        ind2 = data.rfind('-----END PUBLIC KEY-----')
        pubKey = data[ind1+1:ind2]
        f.close()
    except IOError:
        print("Erreur durant l'ouverture ou lecture du fichier 38")
        sys.exit()
    else:
        f.close()
        return pubKey



def hashData(data): 
    try:
        tempFile = open("tempHash.txt", "w")
        tempFile.write(str(data))
    except IOError:
            print("Erreur durant l'ouverture ou écriture du fichier 12")
            sys.exit()
    else:
        tempFile.close()
        cmd = "openssl dgst -sha256 tempHash.txt > hashedVal.txt"
        os.system(cmd)
        try:
            f = open("hashedVal.txt", "r")
            hasedData = f.read()
        except IOError:
            print("Erreur durant l'ouverture ou lecture du fichier 9")
            sys.exit()
        else:
            f.close()
            os.system("rm tempHash.txt hashedVal.txt")
            h = hasedData.split("=")
            ha = h[1].strip()
            return ha

