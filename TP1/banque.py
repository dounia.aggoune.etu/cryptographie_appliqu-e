#!/usr/bin/python3
# coding: utf-8

import sys
import common
import os
import os.path
from os import path
# import openSSL
# from openSSL import crypto
# from openSSL.crypto import X509 #CERTIFICATE DISPLAY AND SIGNING UTILITY
# import base64

def initBanque():
    # create folder /Banque to store accountsBDD, and bank key pair
    if path.exists("Banque/") == False:
            mkdirCmd = "mkdir " + "Banque/"
            os.system(mkdirCmd)
    # generate key pair for bank
    if os.path.isfile("Banque/private.pem") == False: # checks if file already exists or not
        common.genererPaireDeCles("banque", "banque")
        os.system("mv ./Banque/public.pem ./Banque/public-banque.pem")
    if os.path.isfile("Banque/comptesBDD.txt") == False:
        try:
            comptesBDDFile = open("Banque/comptesBDD.txt", "x")
        except IOError:
            print("Erreur durant la création du fichier 0")
            sys.exit()


# permet d'avoir des id utilisateur unique pour vérifier unicité des données
def verifierNomUtilisateur(idUser):
    try:
        comptesBDDFile = open("Banque/comptesBDD.txt", "r")
        for line in comptesBDDFile:
            nom = line.split(":")
            if str(nom[0]) == str(idUser):
                return False
        return True
    except IOError:
      print("Erreur durant l'ouverture ou lecture du fichier 1")
      sys.exit()
    else:
        comptesBDDFile.close()


def creerCompteEnBanque(idUser):
    montantInitial = input("Quel est le montant initial a mettre sur le compte ?\n")
    try:
        comptesBDDFile = open("Banque/comptesBDD.txt", "a")
        comptesBDDFile.write(str(idUser) + ":" + str(montantInitial) + "\n")
    except IOError:
      print("Erreur durant l'ouverture ou écriture du fichier 2")
      sys.exit()
    else:
        comptesBDDFile.close()
        return montantInitial

def creerUtilisateur(userType):
    idUser = input("Entrer le nom de l'utilisateur \n")
    while verifierNomUtilisateur(idUser) == False:
        idUser = input("Ce nom d'utilisateur existe déjà, veuillez choisir un autre nom \n")

    if str(userType) == "client":
        folder = "Client/" + str(idUser) + "/"
    elif str(userType) == "commercant":
        folder = "Commercant/" + str(idUser) + "/"
    else:
        print("utilisateur " + userType + " n'existe pas")
        return -1

    #On crée un dossier pour l'utilisateur  
    os.system("mkdir -p " + folder)
    # On donne la clé public de la banque à l'utilisateur
    if os.path.isdir("Commercant/") == True:
        os.system("cp ./Banque/public-banque.pem Commercant/")
    if os.path.isdir("Client/") == True:
        os.system("cp ./Banque/public-banque.pem Client/")

    # on génère une paire de clés côté utilisateur
    common.initUser(idUser, userType)
    # on va chercher la clé public de l'utilisateur
    pubKey = common.getPubKey(folder + "public.pem")   

    # On signe la clé public de l'utilisateur avec la clé privée de la banque 
    # pour pourvoir certifier plus tard que l'utilisateur est bien inscrit dans cette banque
    signedKey = common.signerDonnee(pubKey, "Banque")

    # On donne la clé public signée par la banque à l'utilisateur 
    try:
        signedPubKeyFile = open(folder + "/clePublicSignee.txt", "w")
        signedPubKeyFile.write(str(signedKey))
    except IOError:
      print("Erreur durant l'ouverture ou écriture du fichier 4")
      sys.exit()
    else:
        signedPubKeyFile.close()

    # On creer le compte en ajoutant l'utilisateur et le montant initial sur son compte dans le fichier bdd de la banque
    montant = creerCompteEnBanque(idUser)
    print("Le compte " + userType + " de " + idUser + " est crée, il contient " + montant + " euros")



# Cette fonction permet de verifier si les données ont été signées par la clé privée correspondant a la clé public qu'on met en argument
def verif_Sign(clePulic, base64sha256, donnee): 
    sign = base64.b64decode(base64sha256)

    cle_file = open(clePulic, "r")
    cle = cle_file.read()
    cle_file.close()

    cle_pub = crypto.load_publickey(crypto.FILETYPE_PEM,cle)

    x509 = X509()
    x509.set_pubkey(cle_pub)

    try:
        openSSL.crypto.verify(x509,sign,donnee, "sha256")
        return True
    except:
        return False


#Cette fonction permet de verifier les cheques
def verifierCheques(file):
    #ouvrir le cheque et prendre les informations dont on va avoir besoin
    try:
        f = open(file)
        id_client = f.readline(1)
        id_commercant = f.readline(2)
        montant = f.readline(3)
        num_facture = f.readline(4)
        num_cheque = f.readline(5)
        hash_infos = f.readline(6)
        hash_signé = f.readline(7)
        cps = f.readline(8)
    except IOError:
        print("Erreur durant l'ouverture ou lecture du fichier 5")
        sys.exit()
    else:
        f.close()

    #vérifier l'appartenance a la banque
    if (verif_Sign(clepubBanque, base64sha256, cps)): #si la signature est valide
        if (verif_Sign("./Client/" + id_client + "/public.pem", base64sha256, hash_infos)): #si le hash est valide
            return True
        else:
            return False
    else:
        return False


def encaisserCheques(check):
    if verifierCheques(check):
        # read check
        try:
            chequeFile = open(check, "r")
            id_client = chequeFile.readline(1)
            id_commercant = chequeFile.readline(2)
            montant = chequeFile.readline(3)
        except IOError:
            print("Erreur durant l'ouverture ou lecture du fichier 6")
            sys.exit()
        else:
            chequeFile.close()

    	#debiter client et ajouter montant sur compte commerçant
        try:
            comptesBDDFile = open("Banque/comptesBDD.txt", "r")
            data = ""
            for line in file:
                idUser = line.split(":")
                print("USER ID after split ======= " + idUser[0])

                if str(idUser[0]) == id_client:
                    balance = int(idUser[1])
                    balance-=montant
                    newBalance = str(idUser[0]) + str(balance) + "\n"
                    data+=newBalance
                elif str(idUser[0]) == id_commercant:
                    balance = int(idUser[1])
                    balance+=montant
                    newBalance = str(idUser[0]) + str(balance) + "\n"
                    data+=newBalance
                else:
                    data+=line.strip() + "\n"
        except IOError:
            print("Erreur durant l'ouverture ou lecture du fichier 7")
            sys.exit()
        else:
            comptesBDDFile.close()
        
        try:
            comptesBDDFile = open("Banque/comptesBDD.txt", "w")
            comptesBDDFile.write(data)
        except IOError:
            print("Erreur durant l'ouverture ou écriture du fichier 8")
            sys.exit()
        else:
            comptesBDDFile.close()
    else:
        print("Le cheque n'a pas pu être encaissé car il n'est pas valide.")
	
	


