#ifndef ALL_USERS_H
#define ALL_USERS_H

class User; 

#include <vector>
#include <string>
#include "user.hpp"
#include "contact.hpp"

class AllUsers { /* Singleton class to store all created users */

   static AllUsers *instance; /* pointeur vers le singleton */
   AllUsers(); /* constructeur */

public:
   std::vector<User*> vector_all_users;
   static AllUsers* getInstance();
   void addUserToNetwork(User* u);
   void removeRoot(User* u);
   User* getUser(std::string name);
   void displayUsers();

   ~AllUsers();
};

#endif
