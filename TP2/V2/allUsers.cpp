#include "allUsers.hpp"

AllUsers *AllUsers::instance = nullptr;

AllUsers::AllUsers() {}

AllUsers* AllUsers::getInstance() {
    if (instance == nullptr) {
        instance = new AllUsers();
    }
    return instance;
}

void AllUsers::displayUsers() {
	for(long unsigned int i=0; i<vector_all_users.size(); i++) {
		std::cout << "Contact List of: " << vector_all_users[i]->m_name << std::endl;
		vector_all_users[i]->showContactTrustChain();
	}
}

void AllUsers::addUserToNetwork(User* u) {
	vector_all_users.push_back(u);
}

User* AllUsers::getUser(std::string name) {
	for(long unsigned int i=0; i<vector_all_users.size(); i++) {
		if(vector_all_users[i]->m_name == name) {
			return vector_all_users[i];
		}
	}
	return nullptr;
}

void AllUsers::removeRoot(User* u) {
	vector_all_users.erase(vector_all_users.begin());
	delete u;
}

AllUsers::~AllUsers() {
	for(long unsigned int i=0; i<vector_all_users.size(); i++) {
		delete vector_all_users[i];
	}
}

