#include <iostream>
#include <fstream>
#include <cstdlib>
#include <algorithm>  
#include <cstring>

#include "user.hpp"


AllUsers* all_users = AllUsers::getInstance();

User::User(std::string name) : m_name(name) {
    m_name = name;

    //create folder to save keys in it
    std::string str("mkdir " + name);
    char * cstr = new char [str.length()+1];
    std::strcpy (cstr, str.c_str());
    std::system(cstr);

    //create keys:
    std::string cmd1("openssl genrsa -out " + name + "/private.pem 2048");
    char * cstr_cmd1 = new char [cmd1.length()+1];
    std::strcpy (cstr_cmd1, cmd1.c_str());
    std::system(cstr_cmd1);

    std::string cmd2("openssl rsa -in " + name + "/private.pem -outform PEM -pubout -out " + name + "/public.pem");
    char * cstr_cmd2 = new char [cmd2.length()+1];
    std::strcpy (cstr_cmd2, cmd2.c_str());
    std::system(cstr_cmd2);

    //read file to get keys inside vars
    std::ifstream file(name + "/public.pem");  
    if(file) {
        std::string line; 
        while(getline(file, line)) { 
            m_pub_key+=line;
            m_pub_key+='\n';
        }
    }

    std::ifstream file1(name + "/private.pem");  
    if(file1) {
        std::string line; 
        while(getline(file, line)) { 
            m_priv_key+=line;
            m_priv_key+='\n';
        }
    }
}

/* Create new user: creating a new user and adding them to our contact list */
User* User::createNewUser(std::string name) {
    User* test = AllUsers::getInstance()->getUser(name);
    if(test == nullptr) { /* checks if user already exist or not */
        //Create new user:
        User* new_user = new User(name);

        /* add our own contact to the annuaire of the new user we just created
        new user sign our pub_key */
        std::string signed_key_1 = new_user->sign(m_pub_key);
        /* create our contact to add to new_user's contact list annuaire */
        Contact me = Contact(m_name, signed_key_1);
        /* add our contact to the new contact's annuaire */
        new_user->m_annuaire.push_back(me);

        /* add the new user to our own contact list annuaire
        sign pub_key of new user with our own private key */
        std::string signed_key = sign(new_user->m_pub_key);
        /* create a contact for the user we created */
        Contact new_contact = Contact(name, signed_key);
        /* add the user to our own contact list */
        m_annuaire.push_back(new_contact);
        std::cout << "user " << m_name << " created user " << new_user->m_name << std::endl;
        return new_user;
    }
    return nullptr;
}

std::string User::sign(std::string item_to_sign) {
    std::string const nomFichier(m_name + "/item_to_sign.txt");
    std::ofstream monFlux(nomFichier.c_str());
    if(monFlux) {
        monFlux << item_to_sign;
    } else {
        std::cout << "ERROR: Cannot open file." << std::endl;
    }

    std::string signed_item = "";
    std::string cmd1("openssl dgst -sha256 -sign " + m_name + "/private.pem -out /tmp/sign.sha256 " + m_name + "/item_to_sign.txt");
    char * cstr_cmd1 = new char [cmd1.length()+1];
    std::strcpy (cstr_cmd1, cmd1.c_str());
    std::system(cstr_cmd1);

    std::string cmd2("openssl base64 -in /tmp/sign.sha256 -out " + m_name + "/signedPubKeyB64");
    char * cstr_cmd2 = new char [cmd2.length()+1];
    std::strcpy (cstr_cmd2, cmd2.c_str());
    std::system(cstr_cmd2);

    std::ifstream file(m_name + "/signedPubKeyB64");  
    if(file) {
        std::string line; 
        while(getline(file, line)) { 
            signed_item+=line;
            signed_item+='\n';
        }
    }
    return signed_item;
}

bool User::verifySignature(std::string signed_item, std::string clear) {
    std::string result = "";
    std::string cmd1("openssl base64 -d -in " + signed_item + " -out /tmp/sign.sha256");
    char * cstr_cmd1 = new char [cmd1.length()+1];
    std::strcpy (cstr_cmd1, cmd1.c_str());
    std::system(cstr_cmd1);

    std::string cmd2("openssl dgst -sha256 -verify " + m_name + "/public.pem -signature /tmp/sign.sha256 " + clear + " > " + m_name + "/res_verif.txt");
    char * cstr_cmd2 = new char [cmd2.length()+1];
    std::strcpy (cstr_cmd2, cmd2.c_str());
    std::system(cstr_cmd2);

    std::ifstream file(m_name + "/res_verif.txt");  
    if(file) {
        std::string line; 
        while(getline(file, line)) { 
            result+=line;
        }
    }

    return result == "Verified OK";
}

std::string User::verifySignatures(std::vector<std::string> &v_chain) {
    std::string signature = "";
    User* user_to_verify = AllUsers::getInstance()->getUser(v_chain[v_chain.size()-1]); 
    if(user_to_verify != nullptr) {  
        for(long unsigned int i=v_chain.size()-2; i>v_chain.size(); i--) { /* we loop through the v_chain to each user name in the chain */
            User* user_who_signs = AllUsers::getInstance()->getUser(v_chain[i]); /* we get the Users out of each username in the chain */
            User* user_who_verify_signature = AllUsers::getInstance()->getUser(v_chain[i-1]); /* we sign the public key of the person we want to add */
            if(user_who_signs != nullptr) {
                signature = user_who_signs->sign(user_to_verify->m_pub_key);
            }
            if(signature != "" && user_who_verify_signature != nullptr && !user_who_verify_signature->verifySignature(signature, user_to_verify->m_pub_key)) { /* we verify the Contact */
                /* we verify the signature */
                return "";
            }
        }
    }
    return user_to_verify->m_pub_key; /* if all the signature of the public key of the person we want to add are ok we can get the public key */
}

bool User::check_user(std::string name_to_search) {
    for(long unsigned int i=0; i<m_annuaire.size(); i++) {
        if(m_annuaire[i].name == name_to_search) {
            return false; //if we already have this user in our contact list
        }
    }
    return true; //if we dont have this user in our contact list
}

bool User::addUser(std::string name_to_search) {
    User* test = AllUsers::getInstance()->getUser(name_to_search);
    if(test != nullptr) { /* checks if user already exists or not */
        if(check_user(name_to_search)) { /* check if we already know user */
        // std::cout << "heloo" << std::endl;
            std::vector<std::string> v;
            std::vector<std::string> empty;
            v.push_back(m_name);
            // std::cout << "HELLLLLOOOO" << std::endl;
            if(searchUser(name_to_search, this, v, empty)) { 
                std::cout << "TO   VERIFY" << std::endl;
                std::string pubKey = verifySignatures(empty);
                if(pubKey != "") {
                    std::cout << "VERIFIED" << std::endl;
                    //add new contact to the annuaire
                    // User* u = AllUsers::getInstance()->getUser(name_to_search);

                    /* add the new user to our own contact list annuaire
                    sign pub_key of new user with our own private key */
                    std::string signed_key = sign(pubKey);
                    std::cout << std::endl;
                    /* create a contact for the user we created */
                    Contact new_contact = Contact(name_to_search, pubKey, empty);
                    /* add the user to our own contact list */
                    m_annuaire.push_back(new_contact);
                    std::cout << "The user: " << m_name << " searched for user " << name_to_search << ", user " << name_to_search << " has been found in the network and has been added to the contact list of user: " << m_name << std::endl;
                    return true;
                } else {
                    std::cout << "The user: " << name_to_search << " does not exist in network so user " << m_name << " has to create it." << std::endl;
                    createNewUser(name_to_search);
                    return false;
                }
            }
        } else {
            std::cout << "You already have user " << name_to_search << " in your contact list!" << std::endl;
        }
    }
    return false;
}

/*Search for a user in our contacts trusted data base and add them to our contact list if found*/
bool User::searchUser(std::string name_to_search, User* current_user, std::vector<std::string> chain, std::vector<std::string> &vec) {
    if(current_user->m_name == name_to_search) {
        vec = chain;
        return true;
    }

    for(int i=0; current_user->m_annuaire.size(); i++) {
        /* verifies if this user is not already in the chain vector - because if it is then it means we already went though it 
        also if we don't check we would have a recursive infinite loop */
        if(current_user->m_name != name_to_search) {
            if(std::find (chain.begin(), chain.end(), current_user->m_annuaire[i].name) == chain.end()) { 
                User* new_current_user = AllUsers::getInstance()->getUser(current_user->m_annuaire[i].name);
                if(new_current_user != nullptr) {
                    chain.push_back(new_current_user->m_name);
                    return searchUser(name_to_search, new_current_user, chain, vec);
                } 
            }
        }
    }
    return false; 
}

/*send a message to one in our trusted contacts*/
void User::sendMessage(std::string receiver) {

}

/*read our messages */
void User::readMessage() {}  

void User::showContactsAll() {
    for(long unsigned int i=0; i<m_annuaire.size(); i++) {
        std::cout << "contact number " << i << std::endl;
        m_annuaire[i].showContactAll();
    }
}  

void User::showContactsNames() {
    for(long unsigned int i=0; i<m_annuaire.size(); i++) {
        m_annuaire[i].showContactsNames();
    }
}

void User::showContactTrustChain() {
    for(long unsigned int i=0; i<m_annuaire.size(); i++) {
        m_annuaire[i].showContactTrustChain();
    }
}

/* find a more direct connection to our contacs, and if we find 2 or more different path, compare public key to add to trust points */
void User::findShorterPath(std::string user_name) {
    /* recalculate connection paths for contacts who have a long trust chain: 
    meaning we can be less confident about their identity
    try to find different paths for the same contact to compare public key
    this could happen regularly but when ? each time we create a new contact ?
    each time a new contact is created or added to the network ?*/
}

User::~User() {
    //delete folder and rsa key files in it
    std::string str("rm -r " + m_name);
    char * cstr = new char [str.length()+1];
    std::strcpy (cstr, str.c_str());
    std::system(cstr);
}

