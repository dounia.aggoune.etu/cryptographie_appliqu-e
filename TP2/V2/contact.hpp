#ifndef CONTACT_H
#define CONTACT_H

#include <string>
#include <vector>
#include <iostream>
#include "contact.hpp"

class Contact {

    public:
    std::string name;
    std::string signed_pub_key;
    std::vector<std::string> trust_chain;

    /* constructeurs */
    Contact(std::string contact_name, std::string signed_key);
    Contact(std::string contact_name, std::string signed_key, std::vector<std::string> chain);

    void showContactAll();
    void showContactsNames();
    void showContactTrustChain();

    ~Contact(); /* destructeur */

};

#endif

