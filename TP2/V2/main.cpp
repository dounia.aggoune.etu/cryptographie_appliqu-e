#include <iostream>
#include <vector>
#include "user.hpp"
#include "allUsers.hpp"
#include <stdio.h>     
#include <stdlib.h>     
#include <time.h> 

using namespace std;

void init_network() {

	//create a root user to start the network (the root user is deleted later)
	cout << "Creation of root user" << endl;
	cout << endl;
	User * root = new User("root");
	// root user creates 2 other users
	cout << "root user creates 2 other users: user a and user b." << endl;
	cout << endl;
	User * a = root->createNewUser("a");
	User * b = root->createNewUser("b");
	cout << "user a created." << endl;
	cout << "user b created." << endl;
	cout << endl;

	//we keep track of all users created by puting them in a vector stored in a singleton class called AllUsers
	AllUsers::getInstance()->addUserToNetwork(root);
	AllUsers::getInstance()->addUserToNetwork(a);
	AllUsers::getInstance()->addUserToNetwork(b);

	cout << "user a adds user b to their contact list" << endl;
	a->addUser(b->m_name);
	cout << endl;
	cout << "user b adds user a to their contact list" << endl;
	b->addUser(a->m_name);
	cout << endl;

	cout << "CONTACT LIST OF USER ROOT =" << endl;
	root->showContactsNames();

	cout << endl;
	cout << endl;
	cout << "ANNUAIRE OF USER b =" << endl;
	b->showContactsNames();
	cout << endl;
	cout << endl;
	cout << "ANNUAIRE OF USER a =" << endl;
	a->showContactsNames();
	cout << endl;
	cout << endl;

	//delete root user so that the network of connection will be decentralised
	cout << "We delete root user from user database and from contact list of user a and b" << endl;
	AllUsers::getInstance()->removeRoot(root);
	
	// remove root from contacts
	a->m_annuaire.erase(a->m_annuaire.begin());
	b->m_annuaire.erase(b->m_annuaire.begin());

	cout << endl;
	cout << endl;
	cout << "ANNUAIRE OF USER b =" << endl;
	b->showContactsNames();
	cout << endl;
	cout << endl;
	cout << "ANNUAIRE OF USER a =" << endl;
	a->showContactsNames();
}

int main(int argc, char *argv[]) {

	srand(time(NULL));

	init_network();

	cout << "Now we have 2 users: A and B who both trust each other." << endl;
	cout << "A and B can create other users and get to know other users created." << endl;

	int i=0;
	char name = 99;
	while(name < 123) {
		std::string s_name(1, name);
		User * test  = AllUsers::getInstance()->vector_all_users[i];
		User * test1 = test->createNewUser(s_name);
		if(test1 != nullptr) {
			name++;
			AllUsers::getInstance()->addUserToNetwork(test1);
		}
		i++;
	}	

	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
	AllUsers::getInstance()->displayUsers();
	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;

	for(int o=0; o<77; o++) {
		char first = rand() % 122 + 97;
		char sec = rand() % 122 + 97;
		std::string s_first(1, first);
		std::string s_sec(1, sec);
		User * test = AllUsers::getInstance()->getUser(s_first);
		if(test != nullptr) {
			test->addUser(s_sec);
		}
	}

	cout << "------------------------------------------------------------------------------------------" << endl;
	AllUsers::getInstance()->displayUsers();
	cout << "------------------------------------------------------------------------------------------" << endl;

	delete AllUsers::getInstance();

	return 0;
}

