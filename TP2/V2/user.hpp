#ifndef USER_H
#define USER_H

class AllUsers;

#include <string>
#include <vector>

#include "contact.hpp"
#include "allUsers.hpp"

class User {

    public:
    std::string m_name; /* username */
    std::string m_pub_key; /* user's public key */
    std::vector<Contact> m_annuaire; /* user's contact list (people they already know and trust their identity) */

    User(std::string name); /* constructor */

    /* create a new user to be part of the network or trusted users
    This can - can only happen if user does not already exists in the network  
    Initially the new user will only have one contact: the user who created it */
    User* createNewUser(std::string name);

    /* this function searches in the whole network if a user exists 
    if the user exists we add them to our contacts list
    if the user does not exist yet in the network we create it */
    bool addUser(std::string name_to_search);
    bool searchUser(std::string name_to_search, User* current_user, std::vector<std::string> chain, std::vector<std::string> &vec);


    /* send an encrypted message to one of our trusted contacts */
    void sendMessage(std::string name);

    /* read our messages */
    void readMessage();

    bool verifySignature(std::string signed_item, std::string clear);
    std::string verifySignatures(std::vector<std::string> &v_chain);

    /* display info about contacts */
    void showContactsAll();
    void showContactsNames();
    void showContactTrustChain();

    /* test if a contact has a shorter path than our current trust chain and compare the public key we have */
    void findShorterPath(std::string user_name);

    ~User();

    private:
    std::string m_priv_key;
    std::string sign(std::string item_to_sign);
    bool check_user(std::string name_to_search);
    
};

#endif
