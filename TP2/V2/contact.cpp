#include "contact.hpp"

Contact::Contact(std::string contact_name, std::string signed_key) : name(contact_name), signed_pub_key(signed_key) {}

Contact::Contact(std::string contact_name, std::string signed_key, std::vector<std::string> chain) : name(contact_name), signed_pub_key(signed_key), trust_chain(chain) {}

void Contact::showContactAll() {
    std::cout << "name = " << name << std::endl;
    std::cout << "signed public key = " << signed_pub_key << std::endl;
    std::cout << "trust_chain = " << std::endl;
    for(long unsigned int i=0; i<trust_chain.size(); i++) {
        std::cout << "trust_chain[ " << i << "] = " << trust_chain[i] << std::endl;
    }
}

void Contact::showContactsNames() {
    std::cout << "name = " << name << std::endl;
}

void Contact::showContactTrustChain() {
    std::cout << name << " trust chain = ";
    for(long unsigned int i=0; i<trust_chain.size(); i++) {
        std::cout << trust_chain[i] << " ";
    }
    std::cout << std::endl;
}

Contact::~Contact() {}


