## idée:

Classes:
- User
- ContactLine (peut être juste une struct)

#### Fichiers:

- user.cpp
- user.hpp

- contactline.cpp
- contactline.hpp

- main.cpp // tests pour montrer que ca marche (ou pas)

Au début on peut créer un user root qui connait deux user et on met en lien les deux users 
puis on supprime le user root (comme on a fait en bash en fait)


### Un objet *user* **contient**:
- un username
- une clé public
- une clé privée
- un tableau (annuaire) contenant des objets contactLine (qui sont le details des users qu'on connait)

### Un objet *user* **peut**:  
1. *createNewUser()*  
	- Créer un nouvel utilisateur qui n'existe pas encore:  
	> Créer un user avec la fonction constructeur de la classe user 
	> Puis ajouter une ligne dans contact list du nouvel utilisateur et de l utilisateur qui a créer le nouvel utilisateur pour qu'ils se connaissent (soient en lien).  
2. *addUser()* 
	- Demander à ajouter un utilisateur qui existe déjà dans le réseau à sa liste de contacts:  
	Parcours recursif de tous les users possibles à partir de ceux qu'on connait déjà pour essayer d'arriver à l'utilisateur qu'on cherche.
3. Lire message
4. Envoyer message

### Un objet *contactLine* peut etre une struct, elle doit **contenir**:
- un username (username_1)
- La clé public de username_1 signée par l'utilisateur qui a cette ligne dans son annuaire 
- Trust_chain: table contenant tous les usernames par qui on est passé pour récupérer le username_1























