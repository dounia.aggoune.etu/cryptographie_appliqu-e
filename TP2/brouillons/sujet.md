# TP2

## SUJET : Transfert de confiance de manière décentralisée

On veut échanger des messages chiffés. On veut pouvoir vérifier la signature des personnes avec qui on échange pour être sûr de l'identité de la personne avec qui on communique.

On a donc besoin de la clé public de la personne avec qui on communique. Comment va t-on obtenir cette clé public ?

On va se baser sur une autre personne de confiance qui va pouvoir certifier de l'identité de la personne avec cette clé public.


Communication "certifiée" avec quelqu'un (on est sûr de parler à la bonne personne car quelqu'un d'autre de confiance nous a certifié que c'est la bonne personne). 
Similaire au lien crée via réseau sociaux.


Application doit faire :

1. Créer un utilisateur
2. Acquérir la confiance
3. Ajouter quelau'un qui est certifié par quelqu'un d'autre
4. Envoyer des messages (échange de fichiers qu'on envoie et qu'on traite)
	- Verification de l'emmetteur du message (verif signature)

BDD avec noms utilisateurs, clé public et clé public de la personne qui a donné la confiance 

L'application doit fonctionner de manière le plus déconnecté possible.

Pour le point de départ on suppose qu'on a confiance.


## IDÉES:

**Fonctionnalities:**

#### - *createUser(username)*
1. Create a folder name with the username
2. Create a file in username folder that will contain the data about all the people that the user knows  
3. Create an rsa key pair for this person and store it in folder

For example one line in the database file: 
> username_1; public key of username_1; username_x   

Here, username_1 is the one person we know, username_x is the person who we knew before knowing username_1 and who certified the identity of username_1
   
username_x will be a concatenated string of all usernames of people who have certified the identity of the person.ß

#### - *search_for_somone(username)*  
1. search in all of our contacts 
1. Ask people we already know if they know the person we want to connect with  
2. If one of them know they send us the public key of the person signed by them  
3. We verifiy the public key signed by the person we already know  
4. We send a connection request:  
. The connection request contains our own public key signed by the person we already know    
. We encrypt it with the public key of the person we want to know and send it to them  

Example:

**F1 wants to know F3  
F1 only know one person: F2  
F2 knows F3**  

*F1.requestToKnowSomeone(F3)*

> **F1 will ask F2: do you know F3?**   
> **F2: yes I know F3, here is F3's public key signed by me and here your public key signed by me.**    
> **F1: verifies F2's signature of F3's public key with F2's public key. If the verification is correct F1 sends its public key signed by F2 to F3.**  
> **F3 receives F1's request.**   
> **F3 verifies F2's signature of F1's public key, if the verification is good then F3 write an answer if he accepts or refuses the connection, encrypts it with F1's public key and sends it to F1.** 
> **F1 receives the encrypted message from F3, decrypts it with his private key.**  


#### - *acceptRequestToKnowSomeone(message)*
1. Decrypt and read connection request
2. Verify signature of key
3. Encrypt and send an answer message saying if yes or no we accept the request

#### - *sendMessage(encryptedMsg)*

#### - *readMessage(encryptedMsg)*

#### - *verifTrustworthiness()*
























