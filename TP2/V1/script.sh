#!/bin/bash

# 1- search_for_someone: searching for someone we want to contact in our contacts' trusted_database 
# 2- send_message 
# 3- read_message  

###################### FUNCTIONS ######################

send_message_from_to() {
	# créer un fichier message, qui contient deja le message user name
	# le destinataire va allez search
	# TODO
	echo "send_message"
}

send_first_message_from_to() {
	# créer un fichier message, qui contient le username du sender 
	# et qui contient la clé public du sender signée par le sender  
	sign_file $1 $2 # $1 = sender $2 = receiver
	echo "$1" >> $2/first_message.txt
	cat $1/signedPubKeyB64 >> $2/first_message.txt
	echo "First message sent from $1 to $2"
}

read_message() {
	echo "reading message"
}


sign_file() {
	openssl dgst -sha256 -sign "$1"/private.pem -out /tmp/sign.sha256 "$2"/public.pem 
	openssl base64 -in /tmp/sign.sha256 -out "$1"/signedPubKeyB64
}

verify_signature() {
	openssl base64 -d -in "$1"/to_verify.txt -out /tmp/sign.sha256
	openssl dgst -sha256 -verify "$2"/public.pem -signature /tmp/sign.sha256 "$3"/public.pem > "$1"/res_verif.txt
	# cat "$1"/res_verif.txt
	test=$(<"$1"/res_verif.txt)
	rm "$1"/res_verif.txt "$1"/to_verify.txt
	if [ "$test" = "Verified OK" ]; then 
		return 0
	else
		return 1
	fi
}

# checks if the contact we are trying to add is not already in our contact list
check_contact() {
	db_file=$1'/trusted_data_base.txt'
	if [ -f "$db_file" ]; then
		while read line; do # on parcourt our own db
			stringarray=($line)
			contact=${stringarray[0]}
			if [ "$contact" = "$2" ]; then 
				echo "You are already in contact with $2"
				return 1
			fi
		done < "$db_file"
	fi
	return 0
}

search_for_someone() {
	# on a un nom, on cherche ce nom dans les databases de nos trusted contacts
	# si on trouve le nom de la personne que l'on cherche dans un des annuaires 
	# d'un de nos contacts on l'ajoute à notre liste de contact
	db_file=$1'/trusted_data_base.txt'
	if [ -f "$db_file" ]; then
		while read line; do # on parcourt our own db
			stringarray=($line)
			contact=${stringarray[0]}
			# verify if contact don't already exist in our own database
			contact_db=$contact'/trusted_data_base.txt'
			# we go through each name in our contacts' db
			if [ -f "$contact_db" ]; then
				j=6
				cpt=1
				while read li; do 
					str_array=($li)
					test=${str_array[0]}
					if [ $2 = $test ]; then
						echo "User $2 found in $contact's trusted_data_base contacts!!!"
						OUTPUT=$(head -n $j $contact_db | tail -6)
						out_array=(${OUTPUT})
						only_key=${out_array[1]}
						NL=$'\n'
						key="${out_array[1]}${NL}${out_array[2]}${NL}${out_array[3]}${NL}${out_array[4]]}${NL}${out_array[5]}${NL}${out_array[6]}"
						echo "$key" > "$1/to_verify.txt"
						if verify_signature $1 $contact $2; then # verify signature 
							echo "Signature verified."
							# sign the key with our own private key 
							sign_file $1 $2
							# and add the new contact info to our db 
							echo "$2" >> "$1"/trusted_data_base.txt
							cat "$1"/signedPubKeyB64 >> "$1"/trusted_data_base.txt
							return 0
						else
							echo "Signature invalid."
						fi
					fi
					if [ $cpt = $j ]; then
						j=$((j+6))
					fi
					cpt=$((cpt+1))
				done < "$contact_db"
			fi
		done < "$db_file"
	fi
	return 1
}


first_exchange() {
	read -p 'who are you looking for ? ' receiver
	if check_contact $1 $receiver; then
		str=""
		if search_for_someone $user $receiver; then 
			echo "Sending friend request."
			send_first_message_from_to $user $receiver 
			search_for_someone $receiver $user
			send_first_message_from_to $receiver $user
			echo "First contact established, you are now in contact with user $receiver"
		else
			echo "User not found."
		fi
	fi
}


create_user(){

	echo "how many users do you want to create?"
	read NBUSERS
	for i in `seq 1 $NBUSERS`;
	do
		echo "User name:" 
		read NAME
		mkdir -p  $NAME
		touch $NAME/trusted_data_base.txt
		# ssh-keygen -f $NAME/sshkey -t rsa
		# generates .pm key so that we can use them to sign
		openssl genrsa -out $NAME/private.pem 2048
		openssl rsa -in $NAME/private.pem -outform PEM -pubout -out $NAME/public.pem

		# put them in root trusted database and put root in their db
	done
}
###################### END FUNCTIONS ######################

read -p 'Enter your username: ' user

if [ ! -d "$user/" ]; then
	read -p "This username does not exist. Press 1 if you wish to create a user." test
	if [ "$test" = "1" ]; then 
		./init
	else
		echo "Goodbye"
		exit 1
	fi
else 
	while :
	do
		echo 'Press <CTRL+C> to exit.'
		echo ''
		echo 'Press 1 to search for someone'
		echo 'Press 2 to send a message to someone we already know.'
		echo 'Press 3 to consult your mailbox.'
		echo 'Press 4 to create a user that you know'
		
		read option

		case $option in
	        "1")
		 		first_exchange $user
	            ;;
	        "2")
	            echo "send a message."	
	            send_message_from_to
				;;
	        "3")
	            echo "read my messages."
		    	read_message
	            ;;
	        "4")
		    	create_user
	            ;;
	        *)
	            echo "Option inconnue."
	            ;;
		esac
	done
fi	










