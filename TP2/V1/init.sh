#!/bin/bash

# creating the root user if not already created
if [ ! -d "root/" ]; then
    mkdir -p root
    touch root/root_db.txt
    ssh-keygen -f root/sshkey_root -t rsa
fi

#creating first users
echo "how many users do you want to create?"
read NBUSERS
for i in `seq 1 $NBUSERS`;
do
    echo "User name:" 
    read NAME
    mkdir -p  $NAME
    touch $NAME/trusted_data_base.txt
    openssl genrsa -out $NAME/private.pem 2048
    openssl rsa -in $NAME/private.pem -outform PEM -pubout -out $NAME/public.pem

    # put them in root trusted database and put root in their db
done

#remove root user 
rm -r root