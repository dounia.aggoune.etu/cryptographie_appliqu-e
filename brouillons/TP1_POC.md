# Proof of Concept

## But de l’application :

Afin de gérer les micropaiements on propose une alternative numérique au chèque : via une application mais possible hors connexion. Un client émet un chèque à un commerçant et le commerçant encaisse le chèque plus tard. 

Le **commerçant** doit pouvoir vérifier la validité des chèques venant des clients :
- Le client est bien client à la banque
- Les informations contenues dans le chèque sont correctes (nom du client et somme d’argent notamment)  

Un commerçant doit payer 1 euro pour encaisser un ou des chèques. 

La **banque** doit pouvoir créer les comptes clients et les comptes commerçants.  
Elle doit s’assurer que le commerçant reçoit toujours son paiement, quitte à mettre le client à découvert. 
La banque vérifie la validité des chèques avant de les encaisser.   

Le seul rôle du **client** est d'émettre un/des chèques. 

*Un chèque* = un fichier numérique de taille raisonnable. Un chèque ne peut pas dépasser une certaine somme.

### Protocole de création des comptes: 

* **Pour un compte client** : 
	- Génère une pair de clés pour le client
	- Signe la clé public du client avec la clé privée de la banque 
	- Envoie la pair de clé avec la clé public signée par la banque au client  

* **Pour un compte commerçant** : 
	- Génère une pair de clé pour le commerçant
	- Signe la clé public du commerçant avec la clé privé de la banque 
	- Envoie la pair de clé dont la clé privée signée par la banque au commerçant

### Protocole de paiement: 
- Le client émet le chèque 
Pour notre example le chèque est fichier text dont le nom est le numéro du chèque et avec écrit dedans: le nom du client, le nom du commerçant et le montant du chèque
- Le client signe le chèque avec la clé de la banque + sa clé ptivée 
- Il envoie le chèque au commerçant 
- Le commerçant vérifie les signatures (du client et de la banque) 
- Il signe tous les chèques un par un 
- Il envoie à la banque 
- La banque vérifie les trois signatures (banque, client, commerçant)
- Elle débite le compte du client

### Protocole de génération de chèque (fonctionalité utilisée uniquement par les clients): 
* Créer le fichier (le chèque)
* Écrit dans le fichier les informations suivantes : 
	- Clé public du client signée par la banque (chiffrée avec la clé privée de la banque), 
	- Nom du client, 
	- Nom du commercant et montant du chèque
* Signe (chiffre avec clé privée) le fichier de chèque
* Envoie le fichier/chèque au commerçant + envoie sa clé public (non chiffrée) 

### Protocole de vérification de chèque (fonctionalité utilisée par les commerçants): 
* Faire une copy du chèque chiffré par le client
* Déchiffrer une des copies avec la clé public du client 
* Déchiffrer la clé public du client qui est chiffré/signée par la banque et la comparer avec la clé public non signée. 
	- Si les deux clés sont identiques le clients apparetient bien à la banque
	- Si les deux clés ne sont pas identiques: le chèque n'est pas valide
* (verifier informations du chèque : nom commerçant, montant)

### Protocole d'encaissement de chèque (fonctionalité utilisée par la banque): 
* Le commerçant doit fournir les cinq éléments suivants :
	- Le check signé par le client (1)
	- La clé public du client (2)
	- Le check signé par le client et signé par le commerçant par dessus (double signature) (3)
	- La clé public du commerçant (4)
	- La clé public du commerçant signée par la banque (5)

* Étapes de vérification :
	1. Déchiffrer avec la clé public du client (2), le chèque signé uniquement par le client (1) 
	2. Déchiffré la première ligne qui contient la clé public du client signée par la banque et la comparer avec la clé public du client (2)
		- Si elles sont identiques on continue à l'étape suivante
		- Si non : le chèque n'est pas valide
	3. Déchiffrer la clé public du commerçant, qui est signée par la banque (5) 
	4. Comparer cette clé avec la clé public du commerçant (4) : 
		- Si elles sont identiques on continue à l'étape suivante
		- Si non : le chèque n'est pas valide
	5. Déchiffrer le chèque du client signé par le commerçant (3)
	6. Comparer cette signature avec la signature du check du client (1) :
		- Si elles sont identiques on peut encaisser le chèque
		- Si non : le chèque n'est pas valide
















